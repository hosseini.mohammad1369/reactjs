import React, { Component } from 'react';
import avatar1 from '../../img/avatar.png';
import avatar2 from '../../img/avatar1_big.png';

class Contact extends Component {
    render() { 
        return ( 
            <div>
                <div className="container mt-3">
  <h2>Contact Page</h2>
  <p>Media objects can also be nested (a media object inside a media object):</p><br />
  <div className="media border p-3">
    <img src={avatar2} alt="John Doe" className="mr-3 mt-3 rounded-circle" style={{width:60}} />
    <div className="media-body">
      <h4>John Doe <small><i>Posted on February 19, 2016</i></small></h4>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
      <div className="media p-3">
        <img src={avatar1} alt="Jane Doe" className="mr-3 mt-3 rounded-circle" style={{width:45}} />
        <div className="media-body">
          <h4>Jane Doe <small><i>Posted on February 20 2016</i></small></h4>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </div>
      </div>  
    </div>
  </div>
</div>
            </div>
         );
    }
}
 
export default Contact;