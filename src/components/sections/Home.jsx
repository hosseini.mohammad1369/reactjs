import React, { Component } from 'react';
import one from '../../img/one.jpeg';
import two from '../../img/two.jpeg';
import three from '../../img/three.jpg';
import four from '../../img/four.jpg';

class Home extends Component {
    render() {
        return (
            <div className='container'>
                <div className="jumbotron text-center">
                    <h1>My First React App</h1>
                    <p>this is Home page</p>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-sm-4">
                            <h2>About Me</h2>
                            <h5>Photo of me:</h5>
                            <div className="fakeimg"><img src={one} alt='pic' /></div>
                            <p>Some text about me in culpa qui officia deserunt mollit anim..</p>
                            <h2>About Me</h2>
                            <h5>Photo of me:</h5>
                            <div className="fakeimg"><img src={two} alt='pic' />g</div>
                            <p>Some text about me in culpa qui officia deserunt mollit anim..</p>
                            <hr className="d-sm-none" />
                        </div>
                        <div className="col-sm-8">
                            <h2>TITLE HEADING</h2>
                            <h5>Title description, Dec 7, 2017</h5>
                            <div className="fakeimg"><img src={three} alt='pic' /></div>
                            <p>Some text..</p>
                            <p>Sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
                            <br />
                            <h2>TITLE HEADING</h2>
                            <h5>Title description, Sep 2, 2017</h5>
                            <div className="fakeimg"><img src={four} alt='pic' /></div>
                            <p>Some text..</p>
                            <p>Sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
                        </div>
                    </div>
                </div>

                <div className="jumbotron text-center">
                    <p>Footer</p>
                </div>
            </div>
        );
    }
}

export default Home;