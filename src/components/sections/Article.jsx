import React, { Component } from 'react';
// import one from '../../img/one.jpeg';
// import { Link } from 'react-router-dom';

class Article extends Component {
    constructor(props) {
		super(props);
		this.state = {user: []};
	}
	
	componentDidMount() {
		fetch('https://content.iconish.ir/content/getallarticle', {
			method: 'POST',
			body: JSON.stringify({
                arttypeid: "37",
                pageNumber: "0",
                pageSize: "10"
			}),
			headers: {
                "appid": 11,
                "Authorization": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwOTEwODk4MTg3NyIsImljb25DcmVkaXQiOiIwOTEwODk4MTg3NyIsImlhdCI6MTU3MjQzNDkyNH0.9lxs7ZUrNJrpeQundOourLIlRbGeik4tFcZnh-0kPvQ",
                "Content-Type": "application/json"
			}
		}).then(response => {
                return response.json();
			}).then(json => {
				this.setState({
					user:json
				});
			});
	}
    render() { 
        return ( 
            <div>

    <ul>
        { this.state.user.map(user => <li>{user.lstArticle.articleid}</li>)}
      </ul>
                {/* <div className="container">
  <h2>Stretched Link in Card</h2>
  <p>Add the .stretched-link className to a link inside the card, and it will make the whole card clickable (the card will act as a link):</p>
  <div className='flex-cards'>
  <div className="card" style={{width:400}}>
    <img className="card-img-top"  src={this.state.user.map(user => {user.articleType.arttypeimage}} alt="Card" style={{width:"100%"}} />
    <div className="card-body">
      <h4 className="card-title">John Doe</h4>
      <p className="card-text">Some example text some example text. John Doe is an architect and engineer</p>
      <Link to='/' className="btn btn-primary stretched-link">See Profile</Link>
    </div>
  </div>
  

  <div className="card mr-3 ml-3" style={{width:400}}>
    <img className="card-img-top" src={one} alt="Card" style={{width:"100%"}} />
    <div className="card-body">
      <h4 className="card-title">John Doe</h4>
      <p className="card-text">Some example text some example text. John Doe is an architect and engineer</p>
      <Link to='/' className="btn btn-primary stretched-link">See Profile</Link>
    </div>
  </div>


  <div className="card" style={{width:400}}>
    <img className="card-img-top" src={one} alt="Card" style={{width:"100%"}} />
    <div className="card-body">
      <h4 className="card-title">John Doe</h4>
      <p className="card-text">Some example text some example text. John Doe is an architect and engineer</p>
      <Link to='/' className="btn btn-primary stretched-link">See Profile</Link>
    </div>
  </div>
  </div>
</div> */}
            </div>
         );
    }
}
 
export default Article;