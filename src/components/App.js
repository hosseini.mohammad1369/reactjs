import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Home from './sections/Home';
import Article from './sections/Article';
import Contact from './sections/Contact';
import Login from './sections/Login';
import './sections/style/style.scss';

// This site has 3 pages, all of which are rendered
// dynamically in the browser (not server rendered).
//
// Although the page does not ever refresh, notice how
// React Router keeps the URL up to date as you navigate
// through the site. This preserves the browser history,
// making sure things like the back button and bookmarks
// work properly.

export default class App extends React.Component {
  render(){
  return (
    <Router>
      <div className='container'>
        <ul>
          <li className="col-sm-3">
            <Link to="/">Home</Link>
          </li>
          <li className="col-sm-3">
            <Link to="/article">Article</Link>
          </li>
          <li className="col-sm-3">
            <Link to="/contact">Contact</Link>
          </li>
          <li className="col-sm-3">
            <Link to="/login">login</Link>
          </li>
        </ul>

        <hr />

        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/article">
            <Article />
          </Route>
          <Route path="/contact">
            <Contact />
          </Route>
          <Route path="/login">
            <Login />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}
}